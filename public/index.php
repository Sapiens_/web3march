<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Ваши результаты сохранены. Спасибо!');
  }
  include('form.php');
  exit();
}

$errors = FALSE;
if (empty($_POST['name'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if (empty($_POST['email'])) {
  print('Заполните e-mail.<br/>');
  $errors = TRUE;
}
if (empty($_POST['year'])) {
  print('Заполните год рождения.<br/>');
  $errors = TRUE;
}
if (empty($_POST['radio-pol'])) {
  print('Выберите пол.<br/>');
  $errors = TRUE;
}
if (empty($_POST['radio-kon'])) {
  print('Выберите количество конечностей.<br/>');
  $errors = TRUE;
}

if (empty($_POST['bio'])) {
  print('Заполните биографию.<br/>');
  $errors = TRUE;
}

$ability_data = ['god', 'clip', 'fly'];
if (empty($_POST['sel'])) {
    print('Выберите способность<br>');
    $errors = TRUE;
}

$ability_insert = [];
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}

if (!isset($_POST['checkbox'])) {
  print('Подтверите согласие.<br/>');
  $errors = TRUE;
}
if ($errors) {
  exit();
}

$user = 'u20949';
$pass = '7502588';
$db = new PDO('mysql:host=localhost;dbname=u20949', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO users (name,year,sex,email,bio,limb,ab_god,ab_fly,ab_clip) VALUES (:name,:year,:sex,:email,:bio,:limb,:ab_god,:ab_fly,:ab_clip)");
  $stmt -> bindParam(':name', $_POST['name']);
  $stmt -> bindParam(':year', $_POST['year']);
  $stmt -> bindParam(':sex', $_POST['sex']);
  $stmt -> bindParam(':email', $_POST['email']);
  $stmt -> bindParam(':bio', $_POST['bio']);
  $stmt -> bindParam(':limb', $_POST['limb']);
  $stmt -> bindParam(':ab_god', $ability_insert['god']);
  $stmt -> bindParam(':ab_fly', $ability_insert['fly']);
  $stmt -> bindParam(':ab_clip', $ability_insert['clip']);
  $stmt -> execute();
}
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1'); 
?>
