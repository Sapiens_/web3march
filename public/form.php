<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <meta charset="utf-8">
  <title> web3march </title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <header>
    Форма
  </header>
  <div id="main-aside-wrapper">
    <div id="cont" class="container">
      <div id="form" class="col-12 order-lg-3 order-sm-2">
        <form action="" method="POST">
          Ваше имя:
          <label>
            <input name="name" placeholder="Введите имя">
          </label>
          <br>
          <br>
          E-mail:
          <label>
            <input type="email" name="email" placeholder="example@mail.ru">
          </label>
          <p>Ваш год рождения:</p>
          <label>
            Год рождения:<br />
                <select name="year">
                    <?php for($i = 1900; $i < 2021; $i++) {?>
                    <option value="<?php print $i; ?>"><?= $i; ?></option>
                    <?php }?>
                </select>
          </label>

          <p>Пол:</p>
          <label>
            <input type="radio" name="radio-pol" checked="checked" value="M">М
          </label>
          <label>
            <input type="radio" name="radio-pol" value="W" />Ж
          </label>

          <p>Количество конечностей</p><br />
          <label>
            <input type="radio" name="radio-kon" value="0" />0
          </label>
          <label>
            <input type="radio" name="radio-kon" value="1" />1
          </label>
          <label>
            <input type="radio" name="radio-kon" value="2" />2
          </label>
          <label>
            <input type="radio" name="radio-kon" value="3" />3
          </label>
          <label>
            <input type="radio" checked="checked" name="radio-kon" value="4" />4
          </label>
          <label>
            <input type="radio" name="radio-kon" value="5" />5
          </label>

          <p>Сверхспособности</p>
          <label>
            <select id="sp" name="sel[]" multiple=multiple>
              <option value="god">Бессмертие</option>
              <option value="clip"> Прохождение сквозь стены</option>
              <option value="fly">Левитация</option>
            </select>
          </label>

          <p>Биография</p>
          <label>
            <textarea placeholder="Ваша биография:" name="bio" rows="6" cols="60"></textarea>
          </label>
          <br>

          <label>
            С <em>контрактом</em> ознакомлен
            <input type="checkbox" name="checkbox" checked="checked">
          </label>
          <br>
          <input type="submit" value="Отправить">
        </form>
      </div>
    </div>
  </div>
</body>
</html>
